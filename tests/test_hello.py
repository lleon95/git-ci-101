import pytest

from mylibrary.hello import print_hello, print_custom

def setup_module():
  # Beginning
  pass

def teardown_module():
  # Closure
  pass

@pytest.fixture
def Message():
  return "My message"

def test_hello():
  print_hello()

def test_custom(Message):
  print_custom(Message)
