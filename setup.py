#!/usr/bin/env python3


from os import path
from setuptools import setup, find_packages

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

name = "MyProject"
version = "0.1.0"
author = "Luis G. Leon Vega"
setup(
    name=name,
    version=version,
    description='My Project',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://klooid.com',
    author=author,
    author_email='dev@klooid.com',
    packages=find_packages(
        exclude=[
            "*.tests",
            "*.tests.*",
            "tests.*",
            "tests"]),
    scripts=[
        'bin/my_project',
    ],
    install_requires=[
        
    ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    python_requires='>=3.6',
    extras_require={
        'test': ['pytest', 'pytest-dependency'],
    },
)
