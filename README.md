# My Project

Build:

```bash
pip3 install .
# Test that the script was installed
my_project
# Test the location
pip3 list | grep MyProject
```

Test:

```bash
pip3 install .[test]
pytest tests/
```
